//
//  LoginVC.swift
//  Smack
//
//  Created by Kelii Martin on 3/4/18.
//  Copyright © 2018 WERUreo. All rights reserved.
//

import UIKit

class LoginVC: UIViewController
{
    ////////////////////////////////////////////////////////////
    // MARK: - View Controller Life Cycle
    ////////////////////////////////////////////////////////////

    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    ////////////////////////////////////////////////////////////
    // MARK: - IBActions
    ////////////////////////////////////////////////////////////
    
    @IBAction func closePressed(_ sender: Any)
    {
        dismiss(animated: true, completion: nil)
    }
    
    ////////////////////////////////////////////////////////////

    @IBAction func createAccountBtnPressed(_ sender: Any)
    {
        performSegue(withIdentifier: TO_CREATE_ACCOUNT, sender: nil)
    }
}
