//
//  ChannelVC.swift
//  Smack
//
//  Created by Kelii Martin on 3/4/18.
//  Copyright © 2018 WERUreo. All rights reserved.
//

import UIKit

class ChannelVC: UIViewController
{
    ////////////////////////////////////////////////////////////
    // MARK: - IBOutlets
    ////////////////////////////////////////////////////////////

    @IBOutlet weak var loginBtn: UIButton!
    
    ////////////////////////////////////////////////////////////
    // MARK: - View Controller Life Cycle
    ////////////////////////////////////////////////////////////

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60
    }

    ////////////////////////////////////////////////////////////
    // MARK: - IBActions
    ////////////////////////////////////////////////////////////

    @IBAction func loginBtnPressed(_ sender: Any)
    {
        performSegue(withIdentifier: TO_LOGIN, sender: nil)
    }
    
    ////////////////////////////////////////////////////////////

    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {}
}
