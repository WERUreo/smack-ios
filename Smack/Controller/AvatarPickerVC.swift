//
//  AvatarPickerVC.swift
//  Smack
//
//  Created by Kelii Martin on 4/22/18.
//  Copyright © 2018 WERUreo. All rights reserved.
//

import UIKit

class AvatarPickerVC: UIViewController
{
    ////////////////////////////////////////////////////////////
    // MARK: - IBOutlets
    ////////////////////////////////////////////////////////////

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    ////////////////////////////////////////////////////////////
    // MARK: - Properties
    ////////////////////////////////////////////////////////////

    var avatarType = AvatarType.dark
    
    ////////////////////////////////////////////////////////////
    // MARK: - View Controller Lifecycle
    ////////////////////////////////////////////////////////////

    override func viewDidLoad()
    {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    ////////////////////////////////////////////////////////////
    // MARK: - IBActions
    ////////////////////////////////////////////////////////////

    @IBAction func backPressed(_ sender: Any)
    {
        dismiss(animated: true, completion: nil)
    }
    
    ////////////////////////////////////////////////////////////

    @IBAction func segmentControlChanged(_ sender: Any)
    {
        avatarType = segmentControl.selectedSegmentIndex == 0 ? AvatarType.dark : AvatarType.light
        collectionView.reloadData()
    }
}

////////////////////////////////////////////////////////////
// MARK: - UICollectionViewDelegate, UICollectionViewDataSource
////////////////////////////////////////////////////////////

extension AvatarPickerVC : UICollectionViewDelegate, UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "avatarCell", for: indexPath) as? AvatarCell
        {
            cell.configureCell(index: indexPath.item, type: avatarType)
            return cell
        }
        return AvatarCell()
    }
    
    ////////////////////////////////////////////////////////////

    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }

    ////////////////////////////////////////////////////////////

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 28
    }
    
    ////////////////////////////////////////////////////////////

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let avatarName = (avatarType == .dark) ? "dark\(indexPath.item)" : "light\(indexPath.item)"
        UserDataService.instance.setAvatarName(avatarName: avatarName)
        self.dismiss(animated: true, completion: nil)
    }
}

////////////////////////////////////////////////////////////
// MARK: - UICollectionViewDelegateFlowLayout
////////////////////////////////////////////////////////////

extension AvatarPickerVC : UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        var numberOfColumns: CGFloat = 3
        if UIScreen.main.bounds.width > 320
        {
            numberOfColumns = 4
        }
        
        let spaceBetweenCells: CGFloat = 10
        let padding: CGFloat = 40
        let cellDimesion = ((collectionView.bounds.width - padding) - (numberOfColumns - 1) * spaceBetweenCells) / numberOfColumns
        
        return CGSize(width: cellDimesion, height: cellDimesion)
    }
}
