//
//  CreateAccountVC.swift
//  Smack
//
//  Created by Kelii Martin on 3/4/18.
//  Copyright © 2018 WERUreo. All rights reserved.
//

import UIKit

class CreateAccountVC: UIViewController
{
    ////////////////////////////////////////////////////////////
    // MARK: - IBOutlets
    ////////////////////////////////////////////////////////////

    @IBOutlet weak var usernameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passTxt: UITextField!
    @IBOutlet weak var userImg: UIImageView!
    
    ////////////////////////////////////////////////////////////
    // MARK: - Properties
    ////////////////////////////////////////////////////////////

    var avatarName = "profileDefault"
    var avatarColor = "[0.5, 0.5, 0.5, 1]"
    
    ////////////////////////////////////////////////////////////
    // MARK: - View Controller Life Cycle
    ////////////////////////////////////////////////////////////

    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    ////////////////////////////////////////////////////////////

    override func viewDidAppear(_ animated: Bool)
    {
        if UserDataService.instance.avatarName != ""
        {
            avatarName = UserDataService.instance.avatarName
            userImg.image = UIImage(named: avatarName)
        }
    }
    
    ////////////////////////////////////////////////////////////
    // MARK: - IBActions
    ////////////////////////////////////////////////////////////

    @IBAction func closePressed(_ sender: Any)
    {
        performSegue(withIdentifier: UNWIND, sender: nil)
    }
    
    ////////////////////////////////////////////////////////////

    @IBAction func createAccountPressed(_ sender: Any)
    {
        guard let name = usernameTxt.text, usernameTxt.text != "" else { return }
        guard let email = emailTxt.text, emailTxt.text != "" else { return }
        guard let pass = passTxt.text, passTxt.text != "" else { return }
        
        AuthService.instance.registerUser(email: email, password: pass)
        { success in
            if success
            {
                print("user registered")
                AuthService.instance.loginUser(email: email, password: pass)
                { success in
                    if success
                    {
                        print("user logged in")
                        AuthService.instance.createUser(name: name, email: email, avatarName: self.avatarName, avatarColor: self.avatarColor)
                        { success in
                            if success
                            {
                                print(UserDataService.instance.name, UserDataService.instance.avatarName)
                                self.performSegue(withIdentifier: UNWIND, sender: nil)
                            }
                        }
                    }
                }
            }
        }
    }
    
    ////////////////////////////////////////////////////////////

    @IBAction func pickAvatarPressed(_ sender: Any)
    {
        performSegue(withIdentifier: TO_AVATAR_PICKER, sender: nil)
    }
    
    ////////////////////////////////////////////////////////////

    @IBAction func pickBGColorPressed(_ sender: Any)
    {
        
    }
}
